package itis.quiz.tests;
import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipFleetManagerAutoTest {
    SpaceshipFleetManager testSpaceshipFleetManager = new CommandCenter();
    static ArrayList<Spaceship> testArr = new ArrayList<>();
    @BeforeEach
    void fillArr(){
        testArr.add(new Spaceship("Foo", 10, 10, 0));
        testArr.add(new Spaceship("Bar", 10, 5, 0));
        testArr.add(new Spaceship("None", 0 ,0 ,0));
    }
    @AfterEach
    void clearArr(){
        testArr.clear();
    }

    @DisplayName("Проверка на самый вооруженный корабль. Такой существует, метод возвращает нужный корабль")
    @Test
    void getMostPowerfulShip_exist(){
        testArr.remove(0);
        Spaceship mostPowerfulShip = testSpaceshipFleetManager.getMostPowerfulShip(testArr);
        Assertions.assertEquals("Bar", mostPowerfulShip.getName());
    }
    @DisplayName("Проверка на самый вооруженный корабль. Таких несколько, метод возвращает первый")
    @Test
    void getMostPowerfulShip_returnFirst(){
        Spaceship mostPowerfulShip = testSpaceshipFleetManager.getMostPowerfulShip(testArr);
        Assertions.assertEquals("Foo", mostPowerfulShip.getName());
    }
    @DisplayName("Проверка на самый вооруженный корабль. Таких нет, метод возвращает null")
    @Test
    void getMostPowerfulShip_returnNull(){
        testArr.remove(0);
        testArr.remove(0);
        Spaceship mostPowerfulShip = testSpaceshipFleetManager.getMostPowerfulShip(testArr);
        Assertions.assertNull(mostPowerfulShip);
    }
    @DisplayName("Возвращение корабля по имени. Нужный корабль существует, метод его возвращает")
    @Test
    void getShipByName_exist(){
        Spaceship foo = testSpaceshipFleetManager.getShipByName(testArr, "Foo");
        Assertions.assertEquals("Foo", foo.getName());
    }
    @DisplayName("Возвращение корабля по имени. Корабля с заданным именем нет. Метод возвращает null")
    @Test
    void getShipByName_null(){
        Spaceship aNull = testSpaceshipFleetManager.getShipByName(testArr, "Null");
        Assertions.assertNull(aNull);
    }
    @DisplayName("Проверка кораблей на достаточный размер грузового трюма. В списке есть такие корабли. Метод возвращает список нужных кораблей")
    @Test
    void getAllShipsWithEnoughCargoSpace_exist(){
        ArrayList<Spaceship> allShipsWithEnoughCargoSpace = testSpaceshipFleetManager.getAllShipsWithEnoughCargoSpace(testArr, 9);
        boolean isTrue = true;
        for (Spaceship spaceship : allShipsWithEnoughCargoSpace){
            if (spaceship.getCargoSpace() < 9){
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue);
    }
    @DisplayName("Проверка кораблей на достаточный размер грузового трюма. В списке нет таких кораблей. Вовращает пустой список")
    @Test
    void getAllShipsWithEnoughCargoSpace_none(){
        ArrayList<Spaceship> allShipsWithEnoughCargoSpace = testSpaceshipFleetManager.getAllShipsWithEnoughCargoSpace(testArr, 11);
        Assertions.assertEquals(allShipsWithEnoughCargoSpace.size(), 0);
    }
    @DisplayName("Проверка мирных кораблей. В списке есть. Метод возвращает нужные корабли")
    @Test
    void getAllCivilianShips_exist(){
        ArrayList<Spaceship> allCivilianShips = testSpaceshipFleetManager.getAllCivilianShips(testArr);
        boolean isTrue = true;
        for (Spaceship spaceship : allCivilianShips){
            if (spaceship.getFirePower() > 0){
                isTrue = false;
                break;
            }
        }
        Assertions.assertTrue(isTrue);
    }
    @DisplayName("Проверка мирных кораблей. В списке таковых нет. Метод возвращает пустой список")
    @Test
    void getAllCivilianShips_none(){
        testArr.remove(2);
        ArrayList<Spaceship> allCivilianShips = testSpaceshipFleetManager.getAllCivilianShips(testArr);
        Assertions.assertEquals(allCivilianShips.size(), 0);
    }
}
