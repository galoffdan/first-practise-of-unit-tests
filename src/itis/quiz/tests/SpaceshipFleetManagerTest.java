package itis.quiz.tests;

import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {
    SpaceshipFleetManager spaceshipFleetManagerTest;
    ArrayList<Spaceship> testArr = new ArrayList<>();

    public static void main(String[] args) {
        double points = 0;
        SpaceshipFleetManagerTest test = new SpaceshipFleetManagerTest(new CommandCenter());
        boolean test1 = test.getAllCivilianShips_CivilianShipsExist_returnedAllCivilianShips();
        boolean test2 = test.getAllCivilianShips_NoCivilianShips_returnedNull();
        boolean test3 = test.getMostPowerfulShip_MostPowerfulShipExist_returnedMostPowerfulShip();
        boolean test4 = test.getMostPowerfulShip_TwoMostPowerfulShips_returnedFirstShip();
        boolean test5 = test.getShipByName_ShipExists_returnedShipWithRightName();
        boolean test6 = test.getShipByName_ShipDoesntExist_returnedNull();
        boolean test7 = test.getAllShipsWithEnoughCargoSpace_neededShipsExists_returnedRightShip();
        boolean test8 = test.getAllShipsWithEnoughCargoSpace_neededShipsDontExists_returnedEmpyList();
        boolean test9 = test.getMostPowerfulShip_AllShipsAreCivillian_returnedNull();
        if (test1) {
            points += 0.5;
        } else {
            System.out.println("Method getAllCivilianShips doesn't return correct civilian ships");
        }
        if (test2) {
            points += 0.5;
        } else {
            System.out.println("Method getAllCivilianShips doesn't return empty list when there aren't any civilian ship");
        }
        if (test3) {
            points += 0.25;
        } else {
            System.out.println("Method getMostPowerfulShip doesn't return most powerful ship");
        }
        if (test4) {
            points += 0.25;
        } else {
            System.out.println("Method getMostPowerfulShip doesn't return first most powerful ship in the list");
        }
        if (test5){
            points += 0.5;
        }
        else {
            System.out.println("Method returnShipByName doesn't return needed ship");
        }
        if (test6){
            points += 0.5;
        } else {
            System.out.println("Method returnShipByName doesn't return null when there isn't any ship with current name");
        }
        if (test7){
            points += 0.5;
        } else {
            System.out.println("Method getAllShipWithEnoughCargoSpace doesn't return correct ships");
        }
        if (test8){
            points += 0.5;
        } else {
            System.out.println("Method getAllShipWithEnoughCargoSpace doesn't return empty list when there aren't any ship with enough cargo space");
        }
        if (test9){
            points += 0.5;
        } else {
            System.out.println("Method getMostPowerfulShip doesn't return null when all ships are civilian");
        }
        if (points == 4){
            System.out.println("Excellent job, all tests is ok!");
        }
    }

    public SpaceshipFleetManagerTest(SpaceshipFleetManager spaceshipFleetManagerTest) {
        this.spaceshipFleetManagerTest = spaceshipFleetManagerTest;
    }

    private boolean getAllCivilianShips_CivilianShipsExist_returnedAllCivilianShips() {
        testArr.add(new Spaceship("Foo", 0, 0, 0));
        testArr.add(new Spaceship("Bar", 0, 0, 0));
        testArr.add(new Spaceship("Fire", 10, 0, 0));
        ArrayList<Spaceship> allCivilianShips = spaceshipFleetManagerTest.getAllCivilianShips(testArr);
        for (Spaceship spaceship : allCivilianShips) {
            if (spaceship.getFirePower() > 0) {
                testArr.clear();
                return false;
            }
        }
        testArr.clear();
        return true;
    }

    private boolean getAllCivilianShips_NoCivilianShips_returnedNull() {
        testArr.add(new Spaceship("Foo", 10, 0, 0));
        testArr.add(new Spaceship("Bar", 10, 0, 0));
        ArrayList<Spaceship> allCivilianShips = spaceshipFleetManagerTest.getAllCivilianShips(testArr);
        if (allCivilianShips.size() == 0) {
            testArr.clear();
            return true;
        }
        testArr.clear();
        return false;
    }

    private boolean getMostPowerfulShip_MostPowerfulShipExist_returnedMostPowerfulShip() {
        testArr.add(new Spaceship("Foo", 10, 0, 0));
        testArr.add(new Spaceship("Bar", 5, 0, 0));
        Spaceship returnedSpaceship = spaceshipFleetManagerTest.getMostPowerfulShip(testArr);
        if (returnedSpaceship.getFirePower() == 10 && returnedSpaceship.getName().equals("Foo")) {
            testArr.clear();
            return true;
        }
        testArr.clear();
        return false;
    }

    private boolean getMostPowerfulShip_TwoMostPowerfulShips_returnedFirstShip() {
        testArr.add(new Spaceship("Foo", 10, 0, 0));
        testArr.add(new Spaceship("Bar", 10, 0, 0));
        Spaceship returnedSpaceship = spaceshipFleetManagerTest.getMostPowerfulShip(testArr);
        if (returnedSpaceship.getFirePower() == 10 && returnedSpaceship.getName().equals("Foo")) {
            testArr.clear();
            return true;
        }
        testArr.clear();
        return false;
    }

    private boolean getMostPowerfulShip_AllShipsAreCivillian_returnedNull() {
        testArr.add(new Spaceship("Foo", 0, 0, 0));
        testArr.add(new Spaceship("Bar", 0, 0, 0));
        Spaceship returnedSpaceship = spaceshipFleetManagerTest.getMostPowerfulShip(testArr);
        if (returnedSpaceship == null) {
            testArr.clear();
            return true;
        }
        testArr.clear();
        return false;
    }

    private boolean getShipByName_ShipExists_returnedShipWithRightName() {
        testArr.add(new Spaceship("Foo", 0, 0, 0));
        testArr.add(new Spaceship("Bar", 0, 0, 0));
        Spaceship returnedSpaceship = spaceshipFleetManagerTest.getShipByName(testArr, "Foo");
        if (returnedSpaceship.getName().equals("Foo") && returnedSpaceship.getFirePower() == 0) {
            testArr.clear();
            return true;
        }
        testArr.clear();
        return false;
    }

    private boolean getShipByName_ShipDoesntExist_returnedNull() {
        testArr.add(new Spaceship("Foo", 0, 0, 0));
        testArr.add(new Spaceship("Bar", 0, 0, 0));
        Spaceship returnedSpaceship = spaceshipFleetManagerTest.getShipByName(testArr, "None");
        if (returnedSpaceship == null) {
            testArr.clear();
            return true;
        }
        testArr.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_neededShipsExists_returnedRightShip() {
        testArr.add(new Spaceship("Foo", 0, 10, 0));
        testArr.add(new Spaceship("Bar", 0, 5, 0));
        ArrayList<Spaceship> returnedSpaceships = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace(testArr, 9);
        for (Spaceship spaceship : returnedSpaceships) {
            if (spaceship.getCargoSpace() < 9) {
                testArr.clear();
                return false;
            }
        }
        testArr.clear();
        return true;
    }

    private boolean getAllShipsWithEnoughCargoSpace_neededShipsDontExists_returnedEmpyList() {
        testArr.add(new Spaceship("Foo", 0, 7, 0));
        testArr.add(new Spaceship("Bar", 0, 5, 0));
        ArrayList<Spaceship> returnedSpaceships = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace(testArr, 9);
        if (returnedSpaceships.size() == 0) {
            testArr.clear();
            return true;
        }
        testArr.clear();
        return false;
    }
}
