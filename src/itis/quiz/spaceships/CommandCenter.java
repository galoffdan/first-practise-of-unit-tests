package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship mostPowerfulShip = null;
        int maxPower = 0;
        for (Spaceship ship : ships){
            if (ship.getFirePower() > maxPower){
                maxPower = ship.getFirePower();
                mostPowerfulShip = ship;
            }
        }
        return maxPower == 0 ? null : mostPowerfulShip;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship : ships){
            if (ship.getName().equals(name)){
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        for (Spaceship ship : ships){
            if (ship.getCargoSpace() >= cargoSize){
                shipsWithEnoughCargoSpace.add(ship);
            }
        }
        return shipsWithEnoughCargoSpace;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> civilanShips = new ArrayList<>();
        for (Spaceship ship : ships){
            if (ship.getFirePower() == 0){
                civilanShips.add(ship);
            }
        }
        return civilanShips;
    }
}
